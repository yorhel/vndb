// @license magnet:?xt=urn:btih:0b31508aeb0634b347b8270c7bee4d411b5d4109&dn=agpl-3.0.txt AGPL-3.0-only
// @source: https://code.blicky.net/yorhel/vndb/src/branch/master/js
// @license magnet:?xt=urn:btih:b8999bbaf509c08d127678643c515b9ab0836bae&dn=ISC.txt ISC
// @source: https://github.com/d3/d3
// SPDX-License-Identifier: AGPL-3.0-only AND ISC

"use strict";

@include .gen/d3.js
@include vn.js

// @license-end
