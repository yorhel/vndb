DROP TYPE item_info_type CASCADE;
DROP VIEW vnt, releasest, producerst CASCADE;
\i sql/schema.sql
\i sql/func.sql
\i sql/perms.sql
